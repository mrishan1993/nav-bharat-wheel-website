# Translation of WordPress.com - Themes - Intergalactic-2 in Czech
# This file is distributed under the same license as the WordPress.com - Themes - Intergalactic-2 package.
msgid ""
msgstr ""
"PO-Revision-Date: 2018-11-22 12:02:03+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: GlotPress/2.4.0-alpha\n"
"Language: cs_CZ\n"
"Project-Id-Version: WordPress.com - Themes - Intergalactic-2\n"

#: functions.php:92
msgid "Dark Green"
msgstr "Tmavě zelená"

#: functions.php:62
msgid "Black"
msgstr "Černá"

#: functions.php:77
msgid "White"
msgstr "Bílá"

#: content.php:14
msgid "Featured"
msgstr "Doporučeno"

#: single.php:27
msgid "Next"
msgstr "Další"

#: single.php:27
msgid "Previous"
msgstr "Předchozí"

#: inc/wpcom-colors.php:108
msgid "Blue"
msgstr "Modré"

#: search.php:16
msgid "Search Results for: %s"
msgstr "Výsledky vyhledávání: %s"

#: inc/wpcom-colors.php:101
msgid "Dark"
msgstr "Tmavé"

#: inc/wpcom-colors.php:87
msgid "Yellow"
msgstr "Žluté"

#: inc/wpcom-colors.php:94
msgid "Orange"
msgstr "Oranžová"

#: inc/wpcom-colors.php:80
msgid "Green"
msgstr "Zelená"

#: inc/wpcom-colors.php:27
msgid "Headings"
msgstr "Nadpisy"

#: functions.php:82 inc/wpcom-colors.php:74
msgid "Purple"
msgstr "Fialové"

#: inc/wpcom-colors.php:20
msgid "Links"
msgstr "Odkazy"

#: inc/template-tags.php:28
msgctxt "post date"
msgid "Posted on %s"
msgstr "Publikováno %s"

#: inc/template-tags.php:33
msgctxt "post author"
msgid "by %s"
msgstr "od %s"

#: header.php:48
msgid "Menu"
msgstr "Menu"

#. Translators: If there are characters in your language that are not supported
#. by Lato, translate this to 'off'. Do not translate into your own language.
#: functions.php:226
msgctxt "Lato font: on or off"
msgid "on"
msgstr "off"

#: header.php:24
msgid "Skip to content"
msgstr "Přejít k obsahu webu"

#: functions.php:173
msgid "Footer 3"
msgstr "Zápatí 3"

#: functions.php:165
msgid "Footer 2"
msgstr "Zápatí 2"

#: functions.php:157
msgid "Footer 1"
msgstr "Zápatí 1"

#: functions.php:148
msgid "Sidebar"
msgstr "Postranní panel"

#: footer.php:40
msgid "Theme: %1$s by %2$s."
msgstr "Šablona: %1$s od %2$s."

#: footer.php:38
msgid "Proudly powered by %s"
msgstr "Používáme %s (v češtině)."

#. translators: used between list items, there is a space after the comma
#: content-single.php:48
msgid "Tags:"
msgstr "Vlastnosti:"

#: footer.php:38
msgid "http://wordpress.org/"
msgstr "http://cs.wordpress.org/"

#. translators: used between list items, there is a space after the comma
#: content-single.php:45 content-single.php:48
msgid ", "
msgstr ", "

#: content-page.php:41 content-single.php:55 content.php:25
msgid "Edit"
msgstr "Upravit"

#: content-page.php:35 content-single.php:36
msgid "Pages:"
msgstr "Stránky:"

#: content-none.php:28
msgid "It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help."
msgstr "Požadovaný obsah se na webu bohužel nenachází. Můžete ale zkusit vyhledávání konkrétního klíčového slova."

#: content-none.php:23
msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr "Omlouváme se, ale zadaný výraz nebyl na webu nalezen. Zkuste prosím hledat znovu nějaké jiné klíčové slovo."

#: content-none.php:19
msgid "Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr "Chystáte se publikovat první příspěvek? <a href=\"%1$s\">Můžete začít rovnou zde</a>."

#: content-none.php:13
msgid "Nothing Found"
msgstr "Nenalezeno"

#: content-author.php:34
msgid "View all posts by %s"
msgstr "Zobrazit archiv autora: %s"

#: content-author.php:28
msgid "Published by %s"
msgstr "Publikoval %s"

#: comments.php:37
msgid "Newer Comments &rarr;"
msgstr "Novější komentáře &rarr;"

#: comments.php:57
msgid "Comments are closed."
msgstr "Komentáře nejsou povoleny."

#: comments.php:36
msgid "&larr; Older Comments"
msgstr "&larr; Starší komentáře"

#: comments.php:35
msgid "Comment navigation"
msgstr "Navigace pro komentáře"

#: 404.php:19
msgid "It looks like nothing was found at this location. Maybe try a search?"
msgstr "Patrně jste chtěli zobrazit stránku, která se na webu bohužel nenachází. Můžete ale zkusit vyhledávání konkrétního klíčového slova."

#: comments.php:28
msgctxt "comments title"
msgid "One thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] "1 komentář: &bdquo;%2$s&ldquo;"
msgstr[1] "%1$s komentáře: &bdquo;%2$s&ldquo;"
msgstr[2] "%1$s komentářů: &bdquo;%2$s&ldquo;"

#: 404.php:15
msgid "Oops! That page can&rsquo;t be found."
msgstr "Omlouváme se, ale stránka nebyla nalezena."
